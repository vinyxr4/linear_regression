from io import StringIO
import numpy as np
from plotData import plotData
from computeCost import computeCost
from gradientDescent import gradientDescent
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

# ======================= Part 1: Plotting =======================
print "Ploting data..."
data = np.loadtxt ("ex1data1.txt", delimiter = ',')
x = np.array (data[:, 0]); y = np.array (data[:, 1])
m = len (y)


# Plot Data
# Note: You have to complete the code in plotData.m
plotData (x, y)

raw_input ("Program paused. Press enter to continue.")

# ======================= Part 2: Gradient descent =======================
print 'Running Gradient Descent ...'

x = np.transpose (np.matrix ((np.ones (m), np.array (data[:, 0]))))


theta = np.zeros ((2, 1))

# compute and display initial cost
initialCost = computeCost (x, y, theta)
print ('Initial cost %f' % initialCost.item (0))

# Some gradient descent settings
iterations = 1500
alpha = 0.01

[theta, J_history] = gradientDescent(x, y, theta, alpha, iterations);

# print theta to screen
print ('Theta found by gradient descent: %f %f' % (theta[0], theta[1]))

# Plot the linear fit
plt.plot (x[:,1], x*theta, '-')
plt.legend (('Training data', 'Linear regression'))
plt.show (block = False)

# Predict values for population sizes of 35,000 and 70,000
predict1 = np.dot ([1, 3.5], theta).item (0)
print ('For population = 35,000, we predict a profit of %f' % (10000 * predict1))

predict2 = np.dot ([1, 7], theta).item (0)
print ('For population = 70,000, we predict a profit of %f' % (10000 * predict2))

raw_input ("Program paused. Press enter to continue.")

# ============= Part 3: Visualizing J(theta_0, theta_1) =============
print 'Visualizing J(theta_0, theta_1) ...'

# Grid over which we will calculate J
theta0_vals = np.linspace (-10, 10, 50);
theta1_vals = np.linspace (-1, 4, 50);

# initialize J_vals to a matrix of 0's
J_vals = np.transpose (np.zeros((len (theta0_vals), len (theta1_vals))))

# Fill out J_vals
for i in range (0, len (theta0_vals)):
    for j in range (0, len (theta1_vals)):
	  t = [[theta0_vals[i]], [theta1_vals[j]]]
	  J_vals[i, j] = computeCost(x, y, t)

# Because of the way meshgrids work in the surf command, we need to 
# transpose J_vals before calling surf, or else the axes will be flipped

J_vals = np.transpose (J_vals)

# Surface plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
theta0_vals, theta1_vals = np.meshgrid (theta0_vals, theta1_vals)
ax.plot_surface(theta0_vals, theta1_vals, J_vals, cmap = cm.jet, linewidth = 0, rstride=1, cstride=1)
ax.set_xlabel('theta_0')
ax.set_ylabel('theta_1')
plt.show()

# Contour plot
#figure;
# Plot J_vals as 15 contours spaced logarithmically between 0.01 and 100
#contour(theta0_vals, theta1_vals, J_vals, logspace(-2, 3, 20))
#xlabel('\theta_0'); ylabel('\theta_1');
#hold on;
#plot(theta(1), theta(2), 'rx', 'MarkerSize', 10, 'LineWidth', 2);

raw_input ('')
