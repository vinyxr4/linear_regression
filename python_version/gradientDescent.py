import numpy as np
from computeCost import computeCost

def gradientDescent(x, y, theta, alpha, num_iters):

    m = len (y)

    J_history = np.zeros ((num_iters, 1))
    temp = np.zeros ((np.size (theta), 1))
    numParameters = np.size (theta)

    for iter in range (0, num_iters):
        for j in range (0, numParameters):
            delta_j = 0;
            for i in range (0, m):
                delta_j += (x[i,:] * theta - y[i]) * x[i, j]
            delta_j /= m;
            temp[j] = theta[j] - alpha * delta_j;
        theta = temp

        J_history[iter] = computeCost (x, y, theta)
        #fprintf('%f \n', J_history(iter));

    return [theta, J_history]
