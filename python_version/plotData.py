import matplotlib.pyplot as plt

def plotData (x, y):

	plt.plot (x, y, 'rx');
	plt.ylabel ('Profit in $10,000s');				#Set the y-axis label
	plt.xlabel ('Population of City in 10,000s');	#Set the x-axis label
	plt.show (block = False);
