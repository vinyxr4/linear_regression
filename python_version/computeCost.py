import numpy as np

def computeCost (x, y, theta):
	m = len (y)
	J = 0
	for i in range (0, m):
		J += ((np.dot (x[i,:], theta)) - y[i]) ** 2 # J = J + (h(x^(i)) - y^(i))^2)
	J /= (2 * m)
	return J